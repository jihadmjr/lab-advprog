package id.ac.ui.cs.advprog.tutorial4.exercise2.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaTestDrive;
import org.junit.Test;

public class PizzaTestDriveTest {
    @Test
    public void testMainMethodTestDrive() {
        PizzaTestDrive.main(new String[0]);
    }
}