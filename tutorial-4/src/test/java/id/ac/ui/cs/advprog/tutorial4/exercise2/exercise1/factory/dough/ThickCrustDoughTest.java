package id.ac.ui.cs.advprog.tutorial4.exercise2.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {
    private Dough dough;

    @Before
    public void setUp() {
        dough = new ThickCrustDough();
    }

    @Test
    public void testThickCrustDoughToStringMethod() {
        assertEquals(dough.toString(), "ThickCrust style extra thick crust dough");
    }
}