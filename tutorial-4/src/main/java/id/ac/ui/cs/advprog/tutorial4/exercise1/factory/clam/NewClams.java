package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class NewClams implements Clams{
    public String toString() {
        return "New Clams";
    }
}
