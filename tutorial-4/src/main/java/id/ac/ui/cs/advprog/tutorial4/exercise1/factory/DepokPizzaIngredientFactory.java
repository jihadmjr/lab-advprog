package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.NewCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.NewClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NewDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.NewSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.NewVeggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;


public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Dough createDough() {
        return new NewDough();
    }

    @Override
    public Sauce createSauce() {
        return new NewSauce();
    }

    @Override
    public Cheese createCheese() {
        return new NewCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        return new Veggies[]{new NewVeggies(), new Onion(), new RedPepper(),
                new Garlic(), new Mushroom()};
    }

    @Override
    public Clams createClam() {
        return new NewClams();
    }
}
