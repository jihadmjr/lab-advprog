package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class NewDough implements Dough{
    public String toString() {
        return "New Dough";
    }
}
