package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class NewCheese implements Cheese{
    public String toString() {
        return "New Cheese";
    }
}
