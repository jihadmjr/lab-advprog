package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class NewVeggies implements Veggies{
    public String toString() {
        return "New Veggies";
    }
}
